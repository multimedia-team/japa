// ----------------------------------------------------------------------------
//
//  Copyright (C) 2004-2018 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __GLOBAL_H
#define __GLOBAL_H


enum 
{
    CB_INPUT,
    CB_GAIN,
    CB_AUTO,
    CB_LINK,
    CB_PEAK,
    CB_AVER,
    CB_MRES,
    CB_MEMX,
    CB_MEMY,
    CB_RESOL,
    CB_WFACT,
    CB_SPEED,
    CB_TRACEA,
    CB_TRACEB,
    CB_TRACEM,
    CB_ASCALE,
    CB_FSCALE,
    CB_ARESPF
};
 
enum
{
    MM_NONE,
    MM_PEAK,
    MM_AVER
};
    
#endif

